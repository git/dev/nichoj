# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit java-pkg-2 subversion

ESVN_REPO_URI="http://svn/site/java-tools/trunk/ant-tasks"

DESCRIPTION="Banta-specific ant tasks"
HOMEPAGE=""

LICENSE=""
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=">=virtual/jdk-1.4"
RDEPEND=">=virtual/jre-1.4
	www-servers/jboss"

S="${WORKDIR}/${PN}"

ant_src_unpack() {
	subversion_src_unpack
}

src_compile() {
	eant
}
src_install() {
	java-pkg_dojar build/dist/BantaTasks.jar

	dodir /usr/share/ant-core/lib
	dosym /usr/share/${PN}/lib/BantaTasks.jar  /usr/share/ant-core/lib/
	dosym /var/lib/jboss/minimal/lib/jnpserver.jar /usr/share/ant-core/lib/

}
