# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="A small, lightweight addressbook that uses libebook, part of evolution-data-server"
HOMEPAGE="http://projects.o-hand.com/contacts"
SRC_URI="http://www.openedhand.com/~chris/contacts-0.1.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~x86"
IUSE=""

RDEPEND="
	>=dev-libs/glib-2.0
	>=x11-libs/gtk+-2.0
	>=gnome-base/libglade-2.0
	>=gnome-extra/evolution-data-server-1.2"


src_install() {
	emake DESTDIR=${D} install
}
