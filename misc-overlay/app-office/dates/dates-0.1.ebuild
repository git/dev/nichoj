# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="A small, lightweight calendar that uses Evolution Data Server as
a backend"
HOMEPAGE="http://projects.o-hand.com/dates"
SRC_URI="http://projects.o-hand.com/sources/dates/${P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~x86"
IUSE=""

RDEPEND="
	>=dev-libs/glib-2.0
	>=x11-libs/gtk+-2.0
	>=gnome-base/libglade-2.0
	>=gnome-extra/evolution-data-server-1.2
	>=gnome-base/gconf-2.0"


src_install() {
	emake DESTDIR=${D} install
}
