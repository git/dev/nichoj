# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit autotools eutils

DESCRIPTION=""
HOMEPAGE=""
SRC_URI="mirror://sourceforge/gvpn-dialer/${P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=""
RDEPEND="net-misc/cisco-vpnclient-3des
	dev-libs/libpcre
	>=gnome-base/libgnomeui-2.0"

src_unpack() {
	unpack ${A}
	cd ${S}
	# Update reference to pcre.h
	# and update vpnc paths to be consistent with Gentoo
	epatch ${FILESDIR}/${P}-gentoo.patch
	# tweak the glade a little
	#cp ${FILESDIR}/gvpn_dialer.glade .
	#epatch ${FILESDIR}/${P}-glade.patch
	eautoreconf
}

src_install() {
	emake DESTDIR=${D} install
}
