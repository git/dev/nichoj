# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit autotools mono

DESCRIPTION="An application that allows you to publish music over the local network, using DAAP"
HOMEPAGE="http://www.snorp.net/log/tangerine"
SRC_URI="http://www.snorp.net/files/tangerine/tangerine-0.2.6.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE="ipod beagle"

DEPEND="dev-util/pkgconfig"
RDEPEND="ipod? ( >=dev-dotnet/ipod-sharp-0.5.16 )
	beagle? ( >=app-misc/beagle-0.1.4 )
	>=dev-dotnet/glade-sharp-2.0
	>=dev-dotnet/gtk-sharp-2.0
	>=net-dns/avahi-0.6.11"

src_unpack() {
	unpack ${A}
	cd ${S}
	epatch ${FILESDIR}/${P}-optionalipod.patch
	eautoconf
}

src_compile() {
	econf $(use_enable ipod) $(use_enable beagle) || die "econf failed"
	emake || die "emake failed"
}

src_install() {
	emake DESTDIR=${D} install || die "emake install failed"
}
