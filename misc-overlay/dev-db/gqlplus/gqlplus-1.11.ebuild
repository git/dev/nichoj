# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="A drop-in replacement for sqlplus with readline support"
HOMEPAGE="http://gqlplus.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="sys-libs/readline"
RDEPEND="sys-libs/readline
	dev-db/oracle-instantclient-sqlplus"

# TODO fix the build system so it doesn't build/install its own libreadline
src_install() {
	dobin ${PN} || die "Failed to install ${PN} to filesystem."
}
