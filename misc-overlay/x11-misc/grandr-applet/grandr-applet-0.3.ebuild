# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

MY_PN=${PN/-/_}
MY_P="${MY_PN}-${PV}"

DESCRIPTION=""
HOMEPAGE="http://dekorte.homeip.net/download/grandr-applet/"
SRC_URI="http://dekorte.homeip.net/download/grandr-applet/${MY_P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=">=gnome-base/libgnomeui-2.0 >=gnome-base/gnome-panel-2.0"
RDEPEND=">=gnome-base/libgnomeui-2.0 >=gnome-base/gnome-panel-2.0"

S="${WORKDIR}/${MY_P}"

src_install() {
	einstall
}
