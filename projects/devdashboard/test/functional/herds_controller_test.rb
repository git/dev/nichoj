require File.dirname(__FILE__) + '/../test_helper'
require 'herds_controller'

# Re-raise errors caught by the controller.
class HerdsController; def rescue_action(e) raise e end; end

class HerdsControllerTest < Test::Unit::TestCase
  fixtures :herds

  def setup
    @controller = HerdsController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new

    @first_id = herds(:first).id
  end

  def test_index
    get :index
    assert_response :success
    assert_template 'list'
  end

  def test_list
    get :list

    assert_response :success
    assert_template 'list'

    assert_not_nil assigns(:herds)
  end

  def test_show
    get :show, :id => @first_id

    assert_response :success
    assert_template 'show'

    assert_not_nil assigns(:herd)
    assert assigns(:herd).valid?
  end

  def test_new
    get :new

    assert_response :success
    assert_template 'new'

    assert_not_nil assigns(:herd)
  end

  def test_create
    num_herds = Herd.count

    post :create, :herd => {}

    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_equal num_herds + 1, Herd.count
  end

  def test_edit
    get :edit, :id => @first_id

    assert_response :success
    assert_template 'edit'

    assert_not_nil assigns(:herd)
    assert assigns(:herd).valid?
  end

  def test_update
    post :update, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'show', :id => @first_id
  end

  def test_destroy
    assert_nothing_raised {
      Herd.find(@first_id)
    }

    post :destroy, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_raise(ActiveRecord::RecordNotFound) {
      Herd.find(@first_id)
    }
  end
end
