require File.dirname(__FILE__) + '/../test_helper'
require 'developers_controller'

# Re-raise errors caught by the controller.
class DevelopersController; def rescue_action(e) raise e end; end

class DevelopersControllerTest < Test::Unit::TestCase
  fixtures :developers

  def setup
    @controller = DevelopersController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new

    @first_id = developers(:first).id
  end

  def test_index
    get :index
    assert_response :success
    assert_template 'list'
  end

  def test_list
    get :list

    assert_response :success
    assert_template 'list'

    assert_not_nil assigns(:developers)
  end

  def test_show
    get :show, :id => @first_id

    assert_response :success
    assert_template 'show'

    assert_not_nil assigns(:developer)
    assert assigns(:developer).valid?
  end

  def test_new
    get :new

    assert_response :success
    assert_template 'new'

    assert_not_nil assigns(:developer)
  end

  def test_create
    num_developers = Developer.count

    post :create, :developer => {}

    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_equal num_developers + 1, Developer.count
  end

  def test_edit
    get :edit, :id => @first_id

    assert_response :success
    assert_template 'edit'

    assert_not_nil assigns(:developer)
    assert assigns(:developer).valid?
  end

  def test_update
    post :update, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'show', :id => @first_id
  end

  def test_destroy
    assert_nothing_raised {
      Developer.find(@first_id)
    }

    post :destroy, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_raise(ActiveRecord::RecordNotFound) {
      Developer.find(@first_id)
    }
  end
end
