require 'rexml/document'

class Herd < ActiveRecord::Base
  include REXML

  has_and_belongs_to_many :developers 

  validates_presence_of :name

  HERDS_XML = '/home/jnichols/checkouts/gentoo-website/xml/htdocs/proj/en/metastructure/herds/herds.xml'

  def Herd.load_from_xml
    file = File.new(HERDS_XML)
    doc = Document.new(file)
    root = doc.root

    root.elements.each('herd') do |herdnode|
      namenode = herdnode.elements['name']
      emailnode = herdnode.elements['email']
      descriptionnode = herdnode.elements['description']
      projectnode = herdnode.elements['maintainingproject']

      herd = Herd.new
      herd.name = namenode.text
      herd.email = emailnode.text unless emailnode.nil?
      herd.description = descriptionnode.text unless descriptionnode.nil?

      # look for maintainers if not part of a project
      if projectnode.nil?
        herdnode.elements.each('maintainer') do |maintainer|
          email = maintainer.elements['email'].text
          developer = Developer.find_by_nickname(email.gsub /@gentoo\.org$/, '')
          unless developer.nil?
            herd.developers.push(developer)
          end
        end
      end
      herd.save!
    end
  end
end
