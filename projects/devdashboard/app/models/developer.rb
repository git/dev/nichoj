require 'rexml/document'

class Developer < ActiveRecord::Base
  include REXML
  
  has_and_belongs_to_many :herds
  has_many :project_members
  validates_presence_of :nickname

  USERINFO = '/home/jnichols/checkouts/gentoo-website/xml/htdocs/proj/en/devrel/roll-call/userinfo.xml'
  PLANET_INI_URL = 'http://sources.gentoo.org/viewcvs.py/*checkout*/planet/configs/planet.ini'

  def cia_rss_url
    "http://cia.navi.cx/stats/author/#{self.handle}/.rss"
  end
  
  def email
    "#{self.nickname}@gentoo.org" unless self.nickname.nil?
  end

  def Developer.load_planet_settings
    open(PLANET_INI_URL) do |file|
      while line = file.gets do
        # if line looks like "[asfasdfadfa]"
        if line =~ /^\[(.*)\]$/ 
          url = $1
          next if url == "Planet" or url == "DEFAULT"
        
          data = {}
          while line = file.gets do
            line.chomp!
            break if line == ""
            # line looks like "key = value"
            line =~ /(.*) = (.*)$/
            data[$1] = $2
          end

          dev = find_by_nickname data['username']
          next if dev.nil?

          dev.hackergotchi = "http://planet.gentoo.org/images/#{data['face']}" unless data['face'].nil?
          #self.blog_rss = url

          dev.save!
        end
      end
    end
  end

  def Developer.load_from_userinfo
    file = File.new USERINFO
    doc = Document.new file

    doc.root.elements.each('user') do |user|
      dev = find_by_nickname user.attributes['username']
      if dev.nil?
        dev = Developer.create
        dev.nickname = user.attributes['username']
      end

      dev.realname = user.elements['realname'].attributes['fullname']
      dev.pgpkey = user.elements['pgpkey'].text
      dev.joined_on = user.elements['joined'].text
      dev.roles = user.elements['roles'].text unless user.elements['roles'].nil?
      dev.location = user.elements['location'].text unless user.elements['location'].nil?
      if user.elements['status']
        dev.status = user.elements['status'].text
      else
        dev.status = 'active'
      end
        
      dev.save!
    end
  end
end
