class HerdsController < ApplicationController
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    @herd_pages, @herds = paginate :herds, :per_page => 10
  end

  def show
    @herd = Herd.find(params[:id])
  end

  def load_from_xml
    Herd.load_from_xml
    redirect_to :action => 'list'
  end
end
