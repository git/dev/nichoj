require 'rexml/document'

class DevelopersController < ApplicationController
  include REXML

  PLANET_INI_URL = 'http://sources.gentoo.org/viewcvs.py/*checkout*/planet/configs/planet.ini'
  
  USERINFO = '/home/nichoj/checkouts/gentoo/xml/htdocs/proj/en/devrel/roll-call/userinfo.xml'

  def index
    actives
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def retired
    @developer_pages, @developers = paginate :developers,
        :per_page => 10, 
        :conditions => [ "status = 'retired'" ] 
    render :action => 'list'
  end

  def list
    @developer_pages, @developers = paginate :developers,
        :per_page => 10,
        :conditions => [ "status = 'active'" ] 
  end

  def show
    if params[:id] =~ /^\d+$/
      @developer = Developer.find(params[:id])
    else
      @developer = Developer.find_by_nickname(params[:id])
    end
  end

  def load_from_planet
    Developer.load_planet_settings
    redirect_to :action => 'list'
  end

  def load_from_userinfo
    Developer.load_from_userinfo
    redirect_to :action => 'list'
  end
end
