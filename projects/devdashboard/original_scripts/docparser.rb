#!/usr/bin/ruby -w
require 'rexml/document'
require 'find'
#require 'sqlite'
#require 'active_record'

#ActiveRecord::Base.establish_connection(:adapter => "sqlite", :database => "./dbfile")

include REXML

def isXml? (filename)
    filename =~ /\.xml$/
end

class Documentation
    attr_reader :role, :title, :uri
    attr_writer :role, :title, :uri
    def initialize(role, title, uri)
        self.role = role
        self.title = title
        self.uri = uri
    end
end

def findDocumentation(target)
    documents = []
    targetdir="/local/home/checkouts/gentoo-website/xml/htdocs/proj/en/java/"
    Dir.glob("#{targetdir}/**/*.xml").each do |filename|
#    Find.find("/home/jnichols/checkouts/gentoo/xml/htdocs/proj/en/java") do |filename|
        file = File.new(filename)
        begin
            doc = Document.new(file)

            root = doc.root
            title_text = nil
            case doc.doctype.system
                when "/dtd/guide.dtd": 
                    title = root.elements['title']
                    title_text = title.text unless title.nil?
                    
                when "/dtd/project.dtd": 
                    longname = root.elements['longname']
                    title_text = longname.text unless longname.nil?
                else next
            end 


            root.elements.each('author') do |author|
                role = author.attributes['title']
                email = author.elements['mail'].attributes['link']
                if email == target
                    documentation = Documentation.new(role,title_text,filename)
                    documents.push documentation
                end
            end
        rescue Exception
            # catching all exceptions probably is bad :)
        end
    end

    return documents
end
