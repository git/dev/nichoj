#!/usr/bin/ruby -w

require 'developer'
require 'herds'
require 'userinfo'
require 'docparser'
require 'planetiniparser'
require 'metadataparser'
require 'projectparser'

include Herds
include UserInfo
include PlanetIniParser
include MetadataHelper
include ProjectHelper

#dev = Developer.new
#
#UserInfo.updateDev(dev)
#Herds.updateDev(dev)
#MetadataHelper.updateDev(dev)
#PlanetIniParser.updateDev(dev)
#dev.documentation = findDocumentation(dev.email)


developers = UserInfo.indexUsers()
#herds = Herds.indexHerds(developers)
#projects = ProjectHelper.indexProjects(developers)

PlanetIniParser.updateDevs(developers)

developers.each do |email, dev|
    dev.print
    puts
end

