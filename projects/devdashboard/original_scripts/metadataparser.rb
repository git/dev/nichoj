#!/usr/bin/ruby -w
require 'rexml/document'
require 'find'

include REXML


module MetadataHelper
    def MetadataHelper.isMetadata?(filename)
        filename =~ /metadata\.xml$/
    end

    class Metadata
        attr_reader :herds, :maintainers, :longdescription, :package
        attr_writer :herds, :maintainers, :longdescription, :package
        def initialize()
            self.herds = Array.new
            self.maintainers = Array.new
        end
    end

    class Maintainer
        attr_reader :email, :name
        attr_writer :email, :name
    end


    def MetadataHelper.parse_metadata (filename)
        metadata = Metadata.new

        file = File.new(filename)
        package = File.basename(File.dirname(filename))
        category = File.basename(File.dirname(File.dirname(filename)))
        fullpackage = "#{category}/#{package}"
        metadata.package = fullpackage

        doc = Document.new(file)
        root = doc.root

        puts "Indexing #{filename}..."
        root.elements.each('herd') do |herd|
            metadata.herds.push herd.text
        end

        root.elements.each('maintainer') do |maintainer_node|
            maintainer = Maintainer.new
            email = maintainer_node.elements['email']
            maintainer.email = email.text unless email.nil?
            name = maintainer_node.elements['name']
            maintainer.name = name.text unless name.nil?

            metadata.maintainers.push maintainer
        end
        return metadata
    end

    def MetadataHelper.parse_directory(directory)
        indexed_metadata = []
        targetdir="/local/home/checkouts/gentoo-portage-tree/dev-java"
        Dir.glob("#{targetdir}/**/*/metadata.xml").each do |filename|
#        Find.find("/local/home/checkouts/gentoo-portage-tree/dev-java") do |filename|
            if isMetadata? filename
                metadata = parse_metadata(filename)
                unless metadata.maintainers.empty? and metadata.herds.empty?
                    indexed_metadata.push(metadata)
                end
            end
        end

        return indexed_metadata
    end

    def MetadataHelper.updateDev(dev)
        indexed_metadata = parse_directory("/local/home/checkouts/gentoo-portage-tree/dev-java")

        if dev.packages.nil?
            dev.packages = []
        end
        indexed_metadata.each do |metadata|
            unless metadata.maintainers.empty?
                metadata.maintainers.each do |maintainer|
                    if dev.email == maintainer.email
                        dev.packages.push metadata.package
                    end
                end
                
            end
        end

    end
end



#indexed_metadata.each do |metadata|
#    puts "* #{metadata.package}"
#    unless metadata.herds.empty?
#        puts "\tHerds:"
#        puts "\t\t#{metadata.herds}" unless metadata.herds.empty?
#    end
#    unless metadata.maintainers.empty?
#        puts "\tMaintainers:"
#            metadata.maintainers.each do |maintainer|
#                puts "\t\tname: #{maintainer.name}"
#                puts "\t\temail: #{maintainer.email}"
#            end
#        puts
#    end
#end
