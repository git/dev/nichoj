#!/usr/bin/ruby -w

require 'rss/2.0'
require 'open-uri'

class Developer
    attr_reader :name, :handle, :blogRss, :pgpkey, :email, :joined, :birthday, :roles, :location, :herds, :hackergotchi, :forumsHandle, :documentation, :packages, :status, :projects, :projectMembership
    attr_writer :name, :handle, :blogRss, :pgpkey, :email, :joined, :birthday, :roles, :location, :herds, :hackergotchi, :forumsHandle, :documentation, :packages, :status, :projectMembership

    def initialize()
        self.herds = Array.new
        self.packages = Array.new
		self.projectMembership = {}
    end

    def ciaRss()
        return "http://cia.navi.cx/stats/author/#{self.handle}/.rss"
    end

    def forumsProfile()
        myhandle = self.forumsHandle
        myhandle = self.handle if myhandle.nil?
        "http://forums.gentoo.org/profile.php?mode=viewprofile&u=#{myhandle}"
    end

    def forumsPosts()
        myhandle = self.forumsHandle
        myhandle = self.handle if myhandle.nil?
        "http://forums.gentoo.org/search.php?search_author=#{myhandle}"
    end

    def myBugsRss()
        "http://bugs.gentoo.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&status_whiteboard_type=allwordssubstr&status_whiteboard=&keywords_type=allwords&keywords=&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&emailreporter1=1&emailtype1=exact&email1=#{self.handle}%40gentoo.org&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&chfieldfrom=&chfieldto=Now&chfieldvalue=&query_based_on=My%20Open%20Bugs&field0-0-0=noop&type0-0-0=noop&value0-0-0=&ctype=rss"
    end

    def parseRssForItems(feed)
        items = []
        begin
            open(feed) do |http|
                rss_source = http.read
                rss = RSS::Parser.parse(rss_source, true)
                if rss.nil?
                    rss = RSS::Parser.parse(rss_source, true, false)
                end
            
                unless rss.nil?
                    items = rss.items
                end
        end
        # catch some errors that could occur...
        rescue Exception
        end

        return items
    end
    def commitItems()
        parseRssForItems(self.ciaRss)
    end
    def blogItems()
        parseRssForItems(self.blogRss)
    end

    def print()
        puts "Name: #{self.name}" unless self.name.nil?
        puts "Username: #{self.handle}" 
        puts "Status: #{self.status}"
        puts "Email: #{self.email}"
        puts "PGP Key: #{self.pgpkey}" unless self.pgpkey.nil?
        puts "Roles: #{self.roles}" unless self.roles.nil?
        unless self.herds.nil?
            puts "Herds: (#{self.herds.size})"
            self.herds.each { |herd| puts "\t#{herd.name}"}
        end
        puts "Joined: #{self.joined}" unless self.joined.nil?
        puts "Birthday: #{self.birthday}" unless self.birthday.nil?
        puts "Location: #{self.location}" unless self.location.nil?
#        unless self.blogRss.nil?
#            puts "Blog RSS: #{self.blogRss}"
#            puts "Blog Posts: "
#            self.blogItems.each { |item| puts "\t#{item.title}" }
#            puts "Hackergotchi: #{self.hackergotchi}" unless self.hackergotchi.nil?
#        end
        #puts "Commits RSS: #{self.ciaRss}"
        #puts "Commits: "
        #self.commitItems.each { |item| puts "\t#{item.title}" }
        #puts "Bugs RSS: #{self.myBugsRss}"
        puts "Forums Profile: #{self.forumsProfile}"
        unless self.packages.empty?
            puts "Packages:"
            self.packages.each { |package| puts "\t#{package}" }
        end
    
		unless self.projectMembership.nil?
			puts "Projects: (#{self.projectMembership.size})"
			self.projectMembership.each do |key, membership|
				puts "\tProject: #{key}"
				puts "\tRole: #{membership.role}" unless membership.role.nil?
				puts "\tDescription: #{membership.description}" unless membership.description.nil?
				puts
			end
			unless self.documentation.nil?
				puts "Documentation:"
	            self.documentation.each do |doc|
	                puts "\t#{doc.title} : #{doc.role}"
	            end
	        end
		end
    end
end
