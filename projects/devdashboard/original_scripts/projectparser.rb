#!/usr/bin/ruby -w

require 'rexml/document'
include REXML

module ProjectHelper

	class ProjectMember
		attr_reader :role, :description, :developer, :project
		attr_writer :role, :description, :developer, :project
		def initialize()
		end
	end

	class Project
		attr_reader :name, :longname, :lastUpdated, :description, :members
		attr_writer :name, :longname, :lastUpdated, :description, :members
		def initialize()
			self.members = []
		end

		def get_member_by_handle(handle)
			
		end
	end


	def yank_single_text(elements, target)
		result = nil
		elements.each(target) do |node|
			result = node.text
		end
		return result
	end

	def yank_attribute(node, target)
		node.attributes[target]
	end

	def ProjectHelper.indexProjects(developers)
		projects = []
	
		target_dir = "/local/home/checkouts/gentoo-website/xml/htdocs/proj/*/**/index.xml"
		Dir.glob(target_dir).each do |index|
			puts "Reading #{index}..."
			project = Project.new()
			doc = Document.new(File.new(index))
			root = doc.root
	

			project.name = yank_single_text(root.elements, 'name')
	
			if project.name.nil?
				puts "No <name> defined in #{index}... trying to guess"
				project.name = File.basename(File.dirname(index))
			end

			project.longname = yank_single_text(root.elements, 'longname')
			project.description = yank_single_text(root.elements, 'description')
	
			root.elements.each('dev') do |devnode|
				developer = developers["#{devnode.text}@gentoo.org"]
				member = ProjectMember.new
				member.role = yank_attribute(devnode, 'role')
				member.description = yank_attribute(devnode, 'description')
				member.developer = developer
				member.project = project
				developer.projectMembership[project.name] = member unless developer.nil?
		
				project.members.push(member)
			end
	
			projects.push(project)
		end
		return projects
	end

	def ProjectHelper.print(projects)
		projects.each do |project|
			puts "*#{project.name}:"
			puts "\tLong Name: #{project.longname}"
			puts "\tDescription: #{project.description}"
				puts "\tMembers:"
			project.members.each do |member|
				puts "\t\tHandle: #{member.handle}"
				puts "\t\tRole: #{member.role}"
				puts "\t\tDescription: #{member.description}"
				puts
			end
		end
	end
end
