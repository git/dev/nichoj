#!/usr/bin/ruby -w
require 'rexml/document'
include REXML

module UserInfo
    def UserInfo.indexUsers() 
        developers = {}
        root = UserInfo.getRoot()

        root.elements.each('user') do |usernode|
            developer = Developer.new

            developer.handle = usernode.attributes['username']
            developer.name = usernode.elements["realname"].attributes["fullname"]
            developer.pgpkey = usernode.elements["pgpkey"].text
            developer.email = usernode.elements["email"].text
            developer.joined = usernode.elements["joined"].text
#            developer.birthday = user.elements["birthday"].text
            rolesnode = usernode.elements["roles"]
            developer.roles = rolesnode.text unless rolesnode.nil?

            statusnode = usernode.elements['status']
            if statusnode.nil?
                developer.status = 'active'
            else
                developer.status = statusnode.text
            end

            locationnode = usernode.elements["location"]
            developer.location = locationnode.text unless locationnode.nil?

            developers[developer.email] = developer
        end
        return developers
    end

    def UserInfo.getRoot()
        file = File.new('/home/nichoj/checkouts/gentoo/xml/htdocs/proj/en/devrel/roll-call/userinfo.xml')
        doc = Document.new(file)
        
        return doc.root
    end

    def UserInfo.parse(handle)
        root = UserInfo.getRoot()
        user = root.elements["user[@username='" + handle + "']"]
        return user
    end

    def UserInfo.dump(handle)
        user = UserInfo.parse(handle)
        puts "username = " + user.attributes["username"]
        puts "realname = " + user.elements["realname"].attributes["fullname"]
        puts "pgpkey = " + user.elements["pgpkey"].text
        puts "email = " + user.elements["email"].text
        puts "joined = " + user.elements["joined"].text
#        puts "birthday = " + user.elements["birthday"].text
        puts "roles = " + user.elements["roles"].text
        puts "location = " + user.elements["location"].text
    end
    def UserInfo.updateDev(developer)
        user = UserInfo.parse(developer.handle)
        developer.name = user.elements["realname"].attributes["fullname"]
        developer.pgpkey = user.elements["pgpkey"].text
        developer.email = user.elements["email"].text
        developer.joined = user.elements["joined"].text
#        developer.birthday = user.elements["birthday"].text
        developer.roles = user.elements["roles"].text
        developer.location =user.elements["location"].text
    end
end
