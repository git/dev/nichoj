#!/usr/bin/ruby -w

module Herds
    class Herd
        attr_reader :name, :description, :email, :packages, :developers
        attr_writer :name, :description, :email, :packages, :developers
        def initialize(name)
            self.name = name
            self.packages = []
            self.developers = []
        end
    end

    def Herds.indexHerds(developers)
        herdsxml = '/home/nichoj/checkouts/gentoo-website/xml/htdocs/proj/en/metastructure/herds/herds.xml'
        file = File.new(herdsxml)
        doc = Document.new(file)
        root = doc.root

        root.elements.each('herd') do |herdnode|
            namenode = herdnode.elements['name']
            emailnode = herdnode.elements['email']
            descriptionnode = herdnode.elements['description']
            projectnode = herdnode.elements['maintainingproject']

            herd = Herd.new(namenode.text)
            herd.email = emailnode.text unless emailnode.nil?
            herd.description = descriptionnode.text unless descriptionnode.nil?

            # look for maintainers if not part of a project
            if projectnode.nil?
                herdnode.elements.each('maintainer') do |maintainer|
                    email = maintainer.elements['email'].text
                    developer = developers[email]
                    unless developer.nil?
                        developer.herds.push(herd)
                        herd.developers.push(developer)
                    end
                end
#            else
            end
        end
    end

    def Herds.getHerdsForDev(handle)
        line = nil
        IO.popen("herdstat -n -q -d #{handle}") do |pipe|
            line = pipe.gets
        end
        line.split
    end

    def Herds.updateDev(developer)
        developer.herds = Herds.getHerdsForDev(developer.handle)
    end
end
