class CreateProjects < ActiveRecord::Migration
  def self.up
    create_table :projects do |t|
      t.column :name, :string
      t.column :longname, :string
      t.column :last_updated, :date
      t.column :description, :text
    end
  end

  def self.down
    drop_table :projects
  end
end
