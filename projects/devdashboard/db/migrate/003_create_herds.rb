class CreateHerds < ActiveRecord::Migration
  def self.up
    create_table :herds do |t|
      t.column :name, :string
      t.column :email, :string
      t.column :description, :text
    end

    create_table :developers_herds, :id => false do |t|
      t.column :developer_id, :integer
      t.column :herd_id, :integer
    end
  end

  def self.down
    drop_table :herds
    drop_table :developers_herds
  end
end
