class CreateDevelopers < ActiveRecord::Migration
  def self.up
    create_table :developers do |t|
      t.column :nickname, :string

      t.column :roles, :string
      t.column :realname, :string
      t.column :location, :string
      t.column :pgpkey, :string

      t.column :hackergotchi, :string

      t.column :forum_handle, :string

      t.column :status, :string
      t.column :joined_on, :date
    end
  end

  def self.down
    drop_table :developers
  end
end
