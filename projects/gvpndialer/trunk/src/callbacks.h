#include <gnome.h>
#include <gconf/gconf-client.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <signal.h>
#include <pcre.h>
#include <unistd.h>
#include "eggtrayicon.h"

#define BUFSIZE 1024
/* used by pcre code */
#define OVECCOUNT 30 /* should be a multiple of 3 */

void
on_vpnclient_main_window_show          (GtkWidget       *widget,
                                        gpointer         user_data);

gboolean
on_vpnclient_main_window_delete_event  (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_preferences1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_status2_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_about1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_execute_vpn_button_clicked          (GtkButton       *button,
                                        gpointer         user_data);

void
on_disconnect_button_clicked           (GtkButton       *button,
                                        gpointer         user_data);

void
on_status_button_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_quit_button_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_vpn_profile_entry_box_changed       (GtkEditable     *editable,
                                        gpointer         user_data);

gboolean
on_login_window_delete_event           (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_cancel_button_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_submit_button_clicked               (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_preference_window_delete_event      (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_preference_cancel_button_clicked    (GtkButton       *button,
                                        gpointer         user_data);

void
on_preference_apply_button_clicked     (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_status_window_delete_event          (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_close_status_window_button_clicked  
(GtkButton *button,gpointer user_data);


                                                                                
int
kill_child  ();
                                                                                
int
write_text_into_fd 
(const gchar *my_text_entry,GIOChannel *ioc);
                                                                                                                                        
void
update_statusbar   
(GtkWidget *anywidget,
 gchar *status_message);
                                                                                
int executeVpnClient   
(const gchar *profile,
 GtkWidget *widget);
                                                                                
                                                                                
int getHostnamefromProfile   
(GtkWidget *mywidget);

int getProfiles   
(gchar *dir_path,
 GtkCombo *vpn_profile_entry);

void close_all_io_channels(gpointer mymain_window);

void start_term 
( gchar **args, GtkWidget *main_window);                                                                    

gboolean
iofunc_stdout (GIOChannel *ioc, GIOCondition cond, gpointer data);

int
getStatus  
(GtkWidget *mywidget);
                                                                     
gchar
*checkWhereVpnClientIs   
(gchar **path,
 gchar *file);

void
create_notify_add   
(GtkEntry *entry,
 gchar *gconf_key);
                                                                     
                                                                     
void
update_pref_widget    
(GConfClient *client,
 guint        cnxn_id,
 GConfEntry   *entry,
 gpointer      user_data);

gboolean
is_vpn_connection_active ();

gboolean
is_vpn_connection_active_full (gpointer mainwindow);

void
on_enter_username_activate             (GtkEntry        *entry,
                                        gpointer         user_data);

void
on_enter_password_activate             (GtkEntry        *entry,
                                        gpointer         user_data);

void
on_submit_button_activate              (GtkButton       *button,
                                        gpointer         user_data);

void
on_status_window_show                  (GtkWidget       *widget,
                                        gpointer         user_data);

gboolean updatestats (gpointer data);

void
on_status2_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data);


void
create_menu(GtkWidget *menu);

void
tray_icon_load(GtkWidget *icon, const gchar *file);

void
clicked (GtkWidget *widget, GdkEventButton *event, gpointer data);

void
tray_create (GtkWidget *mainwindow);

void
tray_status_selected(GtkMenuItem *menuitem, gpointer mainwindow);

void
tray_disconnect_selected(GtkMenuItem *menuitem, gpointer mainwindow);

void
tray_connect_selected(GtkMenuItem *menuitem,  gpointer mainwindow);

int check_for_events(GString *buf);

int check_for_string(gchar *pattern, gchar *subject);

void
on_enter_group_password_activate       (GtkEntry        *entry,
                                        gpointer         user_data);

void
on_group_pw_cancel_button_clicked      (GtkButton       *button,
                                        gpointer         user_data);

void
on_group_pw_submit_button_clicked      (GtkButton       *button,
                                        gpointer         user_data);
static gint
sort_node (gchar* a, gchar *b);

GList*
glist_strings_sort (GList * list);

int
is_network_connection_active();
