#!/usr/bin/ruby -w
require 'zip/zipfilesystem'
require 'getoptlong'

opts = GetoptLong.new(
    [ "--manifest", "-m", GetoptLong::NO_ARGUMENT ],
    [ "--manifestparameter", "-p", GetoptLong::REQUIRED_ARGUMENT ],
    [ "--classes", "-c", GetoptLong::NO_ARGUMENT ],
    [ "--resources", "-r", GetoptLong::NO_ARGUMENT ]
)

domanifest = false
manifestparameter = nil
opts.each do |opt, arg|
    case opt
        when '--manifest'
            domanifest = true
        when '--manifestparameter'
            manifestparameter = arg
    end
end

if ARGV.length != 1
    puts "Expecting one arguement, the location of a jar"
    exit 1
end

zipfilename=ARGV[0]
Zip::ZipFile.open(zipfilename) do |zipfile|
    if domanifest
        if manifestparameter.nil?
            puts zipfile.file.read("META-INF/MANIFEST.MF")
        else
            zipfile.file.open("META-INF/MANIFEST.MF") do |io|
                while line = io.gets
                    line =~ /(.*):\s(.*)$/
                    parameter = $1
                    value = $2
                    if parameter == manifestparameter
                        puts value
                    end
                end
            end
        end
    end
end
