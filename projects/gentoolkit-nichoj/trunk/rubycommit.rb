#!/usr/bin/ruby -w

$: << File.dirname(__FILE__)

module ChangeLog
    def ChangeLog.getLastEntry()
	    f = File.new('ChangeLog')
    	entry=''

       	while line = f.gets
            # TODO parametize this
            if(line.match(/nichoj@gentoo.org/))
                while ! line.match(/:$/)
                    line = f.gets
                end

                while (line = f.gets) && line.strip != ''
                    entry += line.strip + ' '
                end
                break
            end
        end

    	f.close
        return entry.rstrip
    end
end

def isSvn
    File.directory? '.svn'
end

def isCvs
	File.directory? 'CVS'
end

def display_heading(message)
    max = 80
    filler = '#######################'
    remaining = max - filler.length * 2
    leader = (remaining - message.length ) / 2
    puts filler + ' ' * (leader) + message + ' ' * leader + filler
end

def hr
    max = 80
    puts '#' * max
end


def show_keywords()
	display_heading('Keywords')
	system('adjutrix --keyword-graph 2>/dev/null')
end

def show_diff()
	if isSvn or isCvs
		display_heading('Diff')
		if isSvn
		    system('svn diff')
		elsif isCvs
		    system('cvs diff')
		end
		puts 'Exit status: ' + $?.to_s
		return $?
	end
end

def show_repoman
	display_heading 'Repoman'
	system('repoman full')
	puts 'Exit status: ' + $?.to_s
	return $?
end

def show_qualudis
	display_heading 'Qualudis'
	system('qualudis')
	puts 'Exit status: ' + $?.to_s
	return $?
end

def get_logentry()
	# use log entry from command line
	logentry = ARGV[0]
	# or default to your last ChangeLog entry
	if logentry.nil? and File.file?('ChangeLog')
	    logentry = ChangeLog.getLastEntry()
	end
	return logentry
end

def show_logentry(logentry)
	display_heading('ChangeLog')
	puts logentry
	display_heading('Continue? (yes/no)')
end

show_keywords
show_diff or exit 1
show_qualudis or exit 1
show_repoman or exit 1


logentry = get_logentry()

# escape the log entry
escaped = logentry.gsub("'") { "\\'" }

show_logentry(logentry)


line = gets
if line.match(/y|yes/i)
    display_heading('Committing...')
	system ('emanifest *.ebuild')
    if isSvn
        system("svn commit -m \"#{escaped}\"")
    else
        system("repoman commit -m \"#{escaped}\"")
    end
end
