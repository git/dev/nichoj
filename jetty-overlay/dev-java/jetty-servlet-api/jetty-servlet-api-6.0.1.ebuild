# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit java-pkg-2 java-ant-2

DESCRIPTION=""
HOMEPAGE=""
SRC_URI="mirror://jetty-6.0.1-gentoo.tar.bz2"

LICENSE=""
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=">=virtual/jdk-1.4
	dev-java/ant-core"
RDEPEND=">=virtual/jre-1.4"

S="${WORKDIR}/jetty-6.0.1/modules/servlet-api-2.5"

src_compile() {
	eant
}

src_install() {
	java-pkg_newjar target/servlet-api-2.5-${PV}.jar
}
