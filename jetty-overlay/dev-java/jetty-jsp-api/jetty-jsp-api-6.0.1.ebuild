
# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit java-pkg-2 java-ant-2

DESCRIPTION=""
HOMEPAGE=""
SRC_URI="mirror://jetty-6.0.1-gentoo.tar.bz2"

LICENSE=""
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=">=virtual/jdk-1.4
	dev-java/ant-core"
RDEPEND=">=virtual/jre-1.4"

S="${WORKDIR}/jetty-6.0.1/modules/jetty"

src_unpack() {
	unpack ${A}
	cd ${S}
	java-ant_rewrite-classpath build.xml
	rm -r src/test/java/*
}

src_compile() {
	eant \
		-Dgentoo.classpath=$(java-pkg_getjars jetty-servlet-api,jetty-util) \
		-Dmaven.mode.offline=true
}

src_install() {
	java-pkg_newjar target/jetty-${PV}.jar jetty.jar
}

