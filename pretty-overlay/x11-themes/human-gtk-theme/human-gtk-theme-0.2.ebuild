# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Human GTK+ and Metacity Theme"
HOMEPAGE=""
SRC_URI="http://librarian.launchpad.net/4167695/${PN}_${PV}.orig.tar.gz"

LICENSE="CCPL-Attribution-ShareAlike-2.5"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="x11-themes/gtk-engines-ubuntulooks"

src_install() {
	einstall || die "einstall failed"
}
