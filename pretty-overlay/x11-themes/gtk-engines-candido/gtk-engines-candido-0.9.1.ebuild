# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

MY_PN="candido-engine"
MY_P="${MY_PN}-${PV}"

DESCRIPTION=""
HOMEPAGE=""
THEME_URI="http://candido.berlios.de/media/download_gallery"
SRC_URI="
	http://download.berlios.de/candido/${MY_P}.tar.bz2
	http://download.berlios.de/candido/Candido-Engine-Metacity.tar.bz2
	${THEME_URI}/Candido-Selected.tar.bz2
	${THEME_URI}/Candido-Redux.tar.bz2
	http://download.berlios.de/candido/Candido-Engine-Xfwm4.tar.bz2
	${THEME_URI}/Candido-Xfwm4.tar.bz2
	${THEME_URI}/Candido.tar.bz2
	${THEME_URI}/Candido-Graphite.tar.bz2
	${THEME_URI}/Candido-Calm.tar.bz2
	${THEME_URI}/Candido-Candy.tar.bz2
	${THEME_URI}/Candido-DarkOrange.tar.bz2
	${THEME_URI}/Candido-Hybrid.tar.bz2
	${THEME_URI}/Candido-NeoGraphite.tar.bz2
	${THEME_URI}/Candido-Gdm.tar.bz2
	${THEME_URI}/Candido-Walls.tar.bz2"

LICENSE=""
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=">=x11-libs/gtk+-2.8.0"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${MY_P}"

src_compile() {
	econf --enable-animation || die "conf failed"
	emake || die "make failed"
}

src_install() {
	emake DESTDIR=${D} install || die "install failed"

	cd ${WORKDIR}
	dotheme Candido*

	# TODO doesn't seem to be installing the right thing
	dodir /usr/share/gdm/themes
	insinto /usr/share/gdm/themes
	doins -r Candido

	# TODO install xml into /usr/share/gnome-background-properties/
	dodir /usr/share/backgrounds
	insinto /usr/share/backgrounds
	doins *.jpg
}

dotheme() {
	dodir /usr/share/themes
	insinto /usr/share/themes
	doins -r ${@}
}
