# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Community Wallpapers for Edgy"
HOMEPAGE="http://librarian.launchpad.net/4903865/edgy-community-wallpapers_0.5.orig.tar.gz"
SRC_URI="http://librarian.launchpad.net/4903865/edgy-community-wallpapers_0.5.orig.tar.gz"

LICENSE="CCPL-Attribution-ShareAlike-2.5"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

src_install() {
	einstall || die "einstall failed"
}
