# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="The Ubuntu Artwork metapackage."
HOMEPAGE="http://librarian.launchpad.net/3571367/ubuntu-artwork_31.tar.gz"
SRC_URI="http://librarian.launchpad.net/3571367/ubuntu-artwork_31.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~x86"
IUSE=""

RDEPEND="x11-themes/human-cursors-theme
		x11-themes/human-gtk-theme
		x11-themes/human-icon-theme
		x11-themes/edgy-wallpapers
		x11-themes/edgy-community-wallpapers"
