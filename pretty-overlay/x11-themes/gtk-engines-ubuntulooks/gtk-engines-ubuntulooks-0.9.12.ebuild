# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

MY_PN="ubuntulooks"
DESCRIPTION="A GTK+ engine derived and polished from Clearlooks"
HOMEPAGE="https://launchpad.net/distros/ubuntu/edgy/+package/gtk2-engines-ubuntulooks"
SRC_URI="http://librarian.launchpad.net/4710249/${MY_PN}/${MY_PN}_${PV}.orig.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=x11-libs/gtk+-2.8"
RDEPEND=">=x11-libs/gtk+-2.8"

S="${WORKDIR}/${MY_PN}-${PV}"

src_install() {
	einstall || die "einstall failed"
}
