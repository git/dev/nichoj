# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Wallpapers for Edgy"
HOMEPAGE="Wallpapers for Edgy"
SRC_URI="http://librarian.launchpad.net/4903861/edgy-wallpapers_0.8.orig.tar.gz"

LICENSE="CCPL-Attribution-ShareAlike-2.5"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

src_install() {
	einstall || die "einstall failed"
}
