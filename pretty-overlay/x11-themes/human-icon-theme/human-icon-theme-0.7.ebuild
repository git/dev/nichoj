# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Icon theme from Ubuntu"
HOMEPAGE="https://launchpad.net/products/human-icon-theme"
SRC_URI="http://librarian.launchpad.net/4921060/${PN}_${PV}.orig.tar.gz"

LICENSE="CCPL-Attribution-ShareAlike-2.5"
SLOT="0"
KEYWORDS="~x86"
IUSE="png"

DEPEND=">=x11-misc/icon-naming-utils-0.7.0"
RDEPEND=">=x11-misc/icon-naming-utils-0.7.0"

src_compile() {
	econf $(use_enable png png-creation) || die "econf failed"
	emake || die "emake failed"
}

src_install() {
	einstall
}
