# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

MY_PN="rezlooks"
MY_P="${MY_PN}-${PV}"
DESCRIPTION=""
HOMEPAGE=""
THEME_URI="http://people.os-zen.net/rezza/themes"
SRC_URI="http://www.gnome-look.org/content/files/39179-${MY_P}.tar.gz
	${THEME_URI}/Rezlooks-Aerials.tar.gz
	${THEME_URI}/Rezlooks-Gilouche.tar.gz
	${THEME_URI}/Rezlooks-Snow.tar.gz
	${THEME_URI}/Rezlooks-candy.tar.gz
	${THEME_URI}/Rezlooks-dark.tar.gz
	${THEME_URI}/Rezlooks-graphite.tar.gz
"

LICENSE=""
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=""
RDEPEND=""

S="${WORKDIR}/${MY_P}"

src_install() {
	emake DESTDIR=${D} install || die "install failed"
	insinto /usr/share/themes
	doins -r ${WORKDIR}/Rezlooks*
}
